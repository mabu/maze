/**
 * This file is to build a basic game loop:
 *  take input
 *  compute next frame (if needed)
 *  display (if needed)
 * */

#include <curses.h>
#undef MOUSE_MOVED
#include <stdlib.h>             /* atexit */
#include <time.h>

#include "context.h"
#include "functions.h"


int main(void)
{
    int c;
    atexit(graph_desinit);
    struct context con = { 0 };
    graph_init(&con);
  MAZE:
    maze_generate(&con);
    while (1) {
        c = getch();
        if ('a' == c)
            goto MAZE;
        if (ERR != c)
            break;
    }

    return 0;
}
