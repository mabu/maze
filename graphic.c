#include <curses.h>
#include <stdlib.h>
#include <time.h>

#include "context.h"
#include "functions.h"

#define WALL ' ' | A_REVERSE
#define CORRIDOR  ' '


static WINDOW *win;

void graph_init(struct context *con)
{
    srand(time(NULL));

    win = initscr();

    /* get terminal size */
    getmaxyx(win, con->wdim[1], con->wdim[0]);

    /* set cursor invisible */
    curs_set(0);
    /* don't show typed chars */
    noecho();
    /* do not buffer lines */
    cbreak();
    /* non blocking getch */
    timeout(1);
    /* enable special key (arrows) */
    keypad(win, TRUE);

}

void graph_desinit(void)
{
    clear();
    endwin();
}

void graph_draw(struct context *con)
{
    int i, j, x, y;

    for (i = 0, x = con->offset[0]; i < con->dim[0];
         ++i, x += (1 + con->size[0])) {
        for (j = 0, y = con->offset[1]; j < con->dim[1];
             ++j, y += (1 + con->size[1])) {

            move(y, x);
            addch(WALL);

            if (con->rooms[j][i].walls[N])
                mvhline(y, x + 1, WALL, con->size[0]);
            else
                mvhline(y, x + 1, CORRIDOR, con->size[0]);

            if (con->rooms[j][i].walls[W])
                mvvline(y + 1, x, WALL, con->size[1]);
            else
                mvvline(y + 1, x, CORRIDOR, con->size[1]);

            if (i + 1 == con->dim[0]) {
                move(y, x + 1 + con->size[0]);
                addch(WALL);
                mvvline(y + 1, x + 1 + con->size[0], WALL, con->size[1]);
            }
        }
        move(y, x);
        addch(WALL);
        mvhline(y, x + 1, WALL, con->size[0]);
    }
    move(y, x);
    addch(WALL);


    refresh();
}
