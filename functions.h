#ifndef H_20190212_FUNCTIONS
#define H_20190212_FUNCTIONS


/* graphic */
void graph_init(struct context *con);
void graph_desinit(void);
void graph_draw(struct context *con);

/* maze */
void maze_generate(struct context *con);


#endif
