#ifndef H_20201016_CONTEXT
#define H_20201016_CONTEXT

enum dir {
    N,
    S,
    E,
    W
};


enum state {
    not_visited,
    visiting,
    visited

};


struct room {
    int walls[4];
    int value;
    enum state state;
};

typedef struct room room_t;

struct search_path {
    int pos[2];
    room_t *room;
    struct search_path * next;
    struct search_path * prev;
};


struct context
{
    // win dimension
    int wdim[2];
    // maze dimension
    int dim[2];
    int size[2];
    int offset[2];
    room_t **rooms;
    struct search_path *root;
};


#endif
