#include "context.h"
#include "functions.h"
#include <stdlib.h>

#if HAVE_LOG
#include <stdio.h>
FILE *flog = NULL;
#define log(...) \
    if (NULL == flog)\
        flog = fopen("maze.log", "w");\
    if (NULL != flog){\
        fprintf(flog, __VA_ARGS__);\
        fflush(flog);}
#else
#define log(...)
#endif


void init_context(struct context *c)
{
    int i, j;
    c->size[0] = 3;             //3
    c->size[1] = 2;             //2

    c->offset[0] = 2;
    c->offset[1] = 2;

    c->dim[0] = (c->wdim[0] - 2 * c->offset[0]) / (c->size[0] + 1);
    c->dim[1] = (c->wdim[1] - 2 * c->offset[1]) / (c->size[1] + 1);

    // allocate
    c->rooms = calloc(c->dim[1], sizeof (room_t *));
    if (!c->rooms)
        exit(1);
    *(c->rooms) = calloc(c->dim[1] * c->dim[0], sizeof (room_t));

    if (!(*c->rooms))
        exit(1);

    for (j = 0; j < c->dim[1]; ++j)
        c->rooms[j] = &(c->rooms[0][j * c->dim[0]]);

    // wall every where
    for (j = 0; j < c->dim[1]; ++j) {
        for (i = 0; i < c->dim[0]; ++i) {
            c->rooms[j][i].walls[N] = 1;
            c->rooms[j][i].walls[S] = 1;
            c->rooms[j][i].walls[E] = 1;
            c->rooms[j][i].walls[W] = 1;
        }
    }
}

void free_context(struct context *c)
{
    if (c) {
        free(*(c->rooms));
        free(c->rooms);
    }
}

struct search_path *create_node(int x, int y, struct context *c)
{
    struct search_path *ret = calloc(1, sizeof *ret);
    if (NULL == ret) {
        log("calloc failed");
        exit(1);
    }

    ret->pos[0] = x;
    ret->pos[1] = y;
    ret->room = &(c->rooms[y][x]);
    return ret;
}


static int get_explorable_neighbors(int x, int y, int dir[4],
                                    struct context *con)
{
    int ret = 0;
    if (x > 0) {
        if (con->rooms[y][x - 1].state == not_visited) {
            dir[ret] = W;
            ++ret;
        }
    }
    if (x < (con->dim[0] - 1)) {
        if (con->rooms[y][x + 1].state == not_visited) {
            dir[ret] = E;
            ++ret;
        }

    }
    if (y > 0) {
        if (con->rooms[y - 1][x].state == not_visited) {
            dir[ret] = N;
            ++ret;
        }
    }
    if (y < (con->dim[1] - 1)) {
        if (con->rooms[y + 1][x].state == not_visited) {
            dir[ret] = S;
            ++ret;
        }

    }
    return ret;
}

void maze_generate(struct context *con)
{
    init_context(con);

    // select first room
    con->root = create_node(rand() % con->dim[0],
                            rand() % con->dim[1], con);

    con->root->room->state = visiting;

    struct search_path *cur = con->root;

    while (1) {

        if (cur->room->state == visited) {
            struct search_path *old = cur;
            // remove room from stack
            cur = cur->prev;
            free(old);
            if (!cur)
                break;
        }
        while (1) {

            graph_draw(con);
            // get available neighbor for visiting
            int dir, op_dir, cnt, pos[2], dirs[4];
            cnt =
                get_explorable_neighbors(cur->pos[0], cur->pos[1], dirs,
                                         con);

            log("%d explorables rooms from %d:%d\n", cnt, cur->pos[0],
                cur->pos[1]);
            if (0 == cnt) {
                log("going up one level\n");
                cur->room->state = visited;
                break;
            }
            // pick a direction
            dir = dirs[rand() % cnt];
            op_dir = 0;

            pos[0] = cur->pos[0];
            pos[1] = cur->pos[1];
            switch (dir) {
            case N:
                pos[1] -= 1;
                op_dir = S;
                log("go exploring N\n");
                break;
            case S:
                pos[1] += 1;
                op_dir = N;
                log("go exploring S\n");
                break;
            case E:
                pos[0] += 1;
                op_dir = W;
                log("go exploring E\n");
                break;
            case W:
                pos[0] -= 1;
                op_dir = E;
                log("go exploring W\n");
            }

            cur->room->walls[dir] = 0;

            cur->next = create_node(pos[0], pos[1], con);
            cur->next->prev = cur;
            cur = cur->next;
            cur->room->state = visiting;

            cur->room->walls[op_dir] = 0;
        }
    }


    graph_draw(con);
    free_context(con);
}

#ifdef MAZE_TEST
#include <stdio.h>
int main(void)
{
    struct context c;
    c.wdim[0] = 16;
    c.wdim[1] = 16;

    run(&c);

    int i, j;

    for (i = 0; i < c.dim[0]; ++i)
        for (j = 0; j < c.dim[1]; ++j)
            c.rooms[j][i].value = 100 * i + j;

    printf("---\n");

    for (j = 0; j < c.dim[1]; ++j)
        for (i = 0; i < c.dim[0]; ++i)
            printf("%d:%d: %03d\n", i, j, c.rooms[j][i].value);
}
#endif
